package innopolis.politov;

import CarShop.Store;
import Models.Order;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        Store store = new Store();
        store.createCar(50000, "kia rio", "t090rk");
        store.sellCar("kia rio", "john", "konner", "+79126535919");
        store.save();
    }
}
