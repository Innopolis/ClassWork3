package Models;

import java.io.Serializable;

/**
 * Created by General on 2/8/2017.
 */
public class Client implements Serializable {
    private String name;
    private String lastName;
    private String phoneNumber;

    public Client(String name, String lastName, String phoneNumber) {
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int hashCode() {
        return getName().hashCode()*21 + getLastName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }

        if(obj instanceof Client){
            return false;
        }

        Client client = (Client)obj;
        if ((this.getName().equals(client.getName()))
                && (this.getLastName().equals(client.getLastName())))
            return true;
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }



}
