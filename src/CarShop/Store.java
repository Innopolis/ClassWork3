package CarShop;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import Models.*;
import datamanager.ReaderManager;

/**
 * Created by General on 2/8/2017.
 */
public class Store {

    private HashMap<Order, Client> contraactList = new HashMap<>(256);
    private HashSet<Car> cars = new HashSet<>();
    private HashSet<Client> clients = new HashSet<>(256);
    private  static final String FILE_CONTRACTS = "file_contracts.txt";
    private  static final String FILE_CARS = "file_cars.txt";
    private  static final String FILE_CLIENTS = "file_clients.txt";

    public void createCar(int price, String model, String regNum){
        Car car = new Car(price, model, regNum);
        cars.add(car);
    }

    public void sellCar(String model, String firstName, String lastName, String phoneNumber ){
        Client client = new Client(firstName, lastName, phoneNumber);
        clients.add(client);

        Car tempCar = null;
        for(Car car :
                cars){
            if(car.getModel().equals(model)){
                car = tempCar;
            }
        }
        if(tempCar != null){
            Random random = new Random();
            Order order = new Order(tempCar, tempCar.getPrice() * 2, random.nextLong(), (short)80 );
            contraactList.put(order, client);
            cars.remove(tempCar);
        }
        else {
            System.out.println("Car was not found");
        }
    }

    public Order getFirstOrder(){
        for (Order order:
             contraactList.keySet()) {
            return order;
        }
        return null;
    }

    public void getOrders(){
        for(Order order:
                contraactList.keySet()){
            System.out.println( order.toString());
        }


    }

    public void getFreeCars(){
        for(Car car:
                cars){
            System.out.println( car.getModel());
        }
    }

    public void  save(){
        ReaderManager.serialize(cars, FILE_CARS);
        ReaderManager.serialize(clients, FILE_CLIENTS);
        ReaderManager.serialize(contraactList, FILE_CONTRACTS);
    }

    public void recover(){
        ArrayList<Car> list = new ArrayList<>();
        ReaderManager.deserialize(FILE_CARS, list);
        for (Car car:
                list) {
            cars.add(car);

        }

        ArrayList<Client> listClient = new ArrayList<>();
        ReaderManager.deserialize(FILE_CLIENTS, listClient);
        for (Client client:
                listClient) {
            clients.add(client);

        }

        ReaderManager.deserialize(FILE_CONTRACTS, contraactList);
    }
}
