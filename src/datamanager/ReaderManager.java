package datamanager;

import CarShop.Store;
import Models.Order;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by General on 2/8/2017.
 */
public class ReaderManager {


    public static void serialize(Collection<? extends Serializable> collection, String fileName){
        try (FileOutputStream fos = new FileOutputStream("file.txt");
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
            for (Serializable serializable:
                    collection) {
                oos.writeObject(serializable);
            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void serialize (Map<? extends Serializable, ? extends  Serializable> map, String fileName){
        try (FileOutputStream fos = new FileOutputStream("file.txt");
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
            for (Map.Entry<? extends  Serializable, ? extends Serializable> serializable:
                    map.entrySet()) {
                oos.writeObject(serializable.getKey());
                oos.writeObject(serializable.getValue());
            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

    public static void deserialize(String fileName, Collection<Serializable> collection){
        try(FileInputStream fis = new FileInputStream("file.txt");
            ObjectInputStream ois = new ObjectInputStream(fis)){
            Serializable serializable;
            while ( (serializable = (Serializable)ois.readObject()) != null){
                collection.add(serializable);
            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public static void deserialize(String fileName, HashMap<? extends  Serializable, ? extends Serializable> collection){
        try(FileInputStream fis = new FileInputStream("file.txt");
            ObjectInputStream ois = new ObjectInputStream(fis)){
                collection = ois.readObject();
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

}
